package lad.api.utils;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

/**
 * 
 * @author Kenneth M.
 *
 */
public class XMLReader {

	public XMLReader(File fXmlFile) throws ParserConfigurationException, SAXException, IOException {

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);

		doc.getDocumentElement().normalize();

		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

		NodeList itemDropDefinitions = doc.getElementsByTagName("ItemDropDefinition");

		if (itemDropDefinitions.getLength() == 0) {
			Utils.showError("Wrong XML File?", "Can't seem to find any ItemDropDefinition nodes");
			return;
		}

		for (int temp = 0; temp < itemDropDefinitions.getLength(); temp++) {
			Node npcNode = itemDropDefinitions.item(temp);
			System.out.println(npcNode.getNodeName() + " : ");
			System.out.println("----------------------------");

			/**
			 * Generate List of Npc Drop Definitions
			 */
			if (npcNode.getNodeType() == Node.ELEMENT_NODE) {
				Element dropDef = (Element) npcNode;
				int npcId = Integer.parseInt(dropDef.getElementsByTagName("id").item(0).getTextContent());
				System.out.println("NPC ID: " + npcId);
				System.out.println("----------------------------");

				/**
				 * Generate List of Constant Drop Definitions For NPC
				 */
				System.out.println("CONSTANT DROPS");
				System.out.println("----------------------------");
				NodeList constantDrops = dropDef.getElementsByTagName("constant");
				for (int i = 0; i < constantDrops.getLength(); i++) {
					Node constantDrop = constantDrops.item(i);
					if (constantDrop.getNodeType() == Node.ELEMENT_NODE) {
						Element constantDef = (Element) constantDrop;

						NodeList constantItems = constantDef.getElementsByTagName("itemDrop");
						for (int i2 = 0; i2 < constantItems.getLength(); i2++) {
							Node constantItem = constantItems.item(i2);
							if (constantItem.getNodeType() == Node.ELEMENT_NODE) {
								Element constantItemDef = (Element) constantItem;
								int itemId = Integer
										.parseInt(constantItemDef.getElementsByTagName("id").item(0).getTextContent());
								int minAmmount = Integer
										.parseInt(constantItemDef.getElementsByTagName("min").item(0).getTextContent());
								int maxAmmount = Integer
										.parseInt(constantItemDef.getElementsByTagName("max").item(0).getTextContent());
								System.out
										.println("Item ID: " + itemId + " Min: " + minAmmount + " Max: " + maxAmmount);
							}
						}
					}
				}
				System.out.println("----------------------------");
				System.out.println("COMMON DROPS");
				System.out.println("----------------------------");
				/**
				 * Generate List of Common Drop Definitions For NPC
				 */
				NodeList commonDrops = dropDef.getElementsByTagName("common");
				for (int i = 0; i < commonDrops.getLength(); i++) {
					Node commonDrop = commonDrops.item(i);
					if (commonDrop.getNodeType() == Node.ELEMENT_NODE) {
						Element commonDef = (Element) commonDrop;

						NodeList commonItems = commonDef.getElementsByTagName("itemDrop");
						for (int i2 = 0; i2 < commonItems.getLength(); i2++) {
							Node commonItem = commonItems.item(i2);
							if (commonItem.getNodeType() == Node.ELEMENT_NODE) {
								Element commonItemDef = (Element) commonItem;
								int itemId = Integer
										.parseInt(commonItemDef.getElementsByTagName("id").item(0).getTextContent());
								int minAmmount = Integer
										.parseInt(commonItemDef.getElementsByTagName("min").item(0).getTextContent());
								int maxAmmount = Integer
										.parseInt(commonItemDef.getElementsByTagName("max").item(0).getTextContent());
								System.out
										.println("Item ID: " + itemId + " Min: " + minAmmount + " Max: " + maxAmmount);
							}
						}
					}
				}

				System.out.println("----------------------------");
				System.out.println("UNCOMMON DROPS");
				System.out.println("----------------------------");

				/**
				 * Generate List of Uncommon Drop Definitions For NPC
				 */
				NodeList uncommonDrops = dropDef.getElementsByTagName("uncommon");
				for (int i = 0; i < uncommonDrops.getLength(); i++) {
					Node uncommonDrop = uncommonDrops.item(i);
					if (uncommonDrop.getNodeType() == Node.ELEMENT_NODE) {
						Element uncommonDef = (Element) uncommonDrop;

						NodeList uncommonItems = uncommonDef.getElementsByTagName("itemDrop");
						for (int i2 = 0; i2 < uncommonItems.getLength(); i2++) {
							Node uncommonItem = uncommonItems.item(i2);
							if (uncommonItem.getNodeType() == Node.ELEMENT_NODE) {
								Element uncommonItemDef = (Element) uncommonItem;
								int itemId = Integer
										.parseInt(uncommonItemDef.getElementsByTagName("id").item(0).getTextContent());
								int minAmmount = Integer
										.parseInt(uncommonItemDef.getElementsByTagName("min").item(0).getTextContent());
								int maxAmmount = Integer
										.parseInt(uncommonItemDef.getElementsByTagName("max").item(0).getTextContent());
								System.out
										.println("Item ID: " + itemId + " Min: " + minAmmount + " Max: " + maxAmmount);
							}
						}
					}
				}

				System.out.println("----------------------------");
				System.out.println("RARE DROPS");
				System.out.println("----------------------------");

				/**
				 * Generate List of Rare Drop Definitions For NPC
				 */
				NodeList rareDrops = dropDef.getElementsByTagName("rare");
				for (int i = 0; i < rareDrops.getLength(); i++) {
					Node rareDrop = rareDrops.item(i);
					if (rareDrop.getNodeType() == Node.ELEMENT_NODE) {
						Element rareDef = (Element) rareDrop;

						NodeList rareItems = rareDef.getElementsByTagName("itemDrop");
						for (int i2 = 0; i2 < rareItems.getLength(); i2++) {
							Node rareItem = rareItems.item(i2);
							if (rareItem.getNodeType() == Node.ELEMENT_NODE) {
								Element rareItemDef = (Element) rareItem;
								int itemId = Integer
										.parseInt(rareItemDef.getElementsByTagName("id").item(0).getTextContent());
								int minAmmount = Integer
										.parseInt(rareItemDef.getElementsByTagName("min").item(0).getTextContent());
								int maxAmmount = Integer
										.parseInt(rareItemDef.getElementsByTagName("max").item(0).getTextContent());
								System.out
										.println("Item ID: " + itemId + " Min: " + minAmmount + " Max: " + maxAmmount);
							}
						}
					}
				}
				System.out.println("----------------------------");

			}
		}

	}
}
