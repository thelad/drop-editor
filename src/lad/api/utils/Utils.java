package lad.api.utils;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * 
 * @author Kenneth M.
 *
 */
public class Utils {

	/**
	 * Loads a custom font from the data/font folder. Font must be either otf or
	 * ttf.
	 * 
	 * @param fontName
	 * @param size
	 */
	public static void setFont(Component c, String fontName, float size) {
		try {
			Font font = Font.createFont(0, Utils.class.getResource("/resources/fonts/" + fontName).openStream());
			GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			genv.registerFont(font);
			font = font.deriveFont(size);
			c.setFont(font);
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Opens the users browser and goes to the specified URL
	 * 
	 * @param url
	 */
	public static void openWebpage(String url) {
		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
		if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			try {
				desktop.browse(new URL(url).toURI());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static ImageIcon getImageIcon(String name) {
		Image img = Toolkit.getDefaultToolkit().createImage(Utils.class.getResource("/resources/img/" + name));
		ImageIcon imageIcon = new ImageIcon(img);
		return imageIcon;
	}

	public static Image getImage(String name) {
		return Toolkit.getDefaultToolkit().createImage(Utils.class.getResource("/resources/img/" + name));
	}
	
	public static void showError(String title, String message){
		JOptionPane.showMessageDialog(null, message, "Error:  " + title, JOptionPane.INFORMATION_MESSAGE);
	}
}
