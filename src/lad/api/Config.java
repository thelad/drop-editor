package lad.api;

import java.awt.Color;
import java.awt.Dimension;

/**
 * 
 * @author Kenneth M
 *
 */

public class Config {

	public static final Dimension FRAME_SIZE = new Dimension(800,500);
	
	public static final Color BACKGROUND_COLOR = new Color(39,39,39);
	public static final Color MENU_COLOR = new Color(51,51,51);
	public static final Color DEFAULT_BTN_COLOR = new Color(255, 255, 255);
	
}
