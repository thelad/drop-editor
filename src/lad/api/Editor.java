package lad.api;

import java.awt.Color;

import javax.swing.UIManager;

import lad.api.components.Frame;
/**
 * 
 * @author Kenneth M.
 *
 */
public class Editor {
	public static Frame app;
	
	public static void main(String args[]){
			UIManager.put("Button.select", new Color(1.0f, 1.0f, 1.0f, 0.05f));
			System.setProperty("awt.useSystemAAFontSettings", "on");
			System.setProperty("swing.aatext", "true");
			
			System.out.println("Starting Editor");
			
			app = new Frame();
			app.setVisible(true);
			app.setLocationRelativeTo(null);
	}
	
}
