package lad.api.components;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import lad.api.Config;
import lad.api.listeners.MenuItemListener;
/**
 * 
 * @author Kenneth M.
 *
 */
public class MenuBar extends JPanel {
	private static final long serialVersionUID = 1L;

	public MenuBar(Frame frame) {
		setLayout(new BorderLayout());
		setBackground(Config.MENU_COLOR);
		JMenuBar b = new JMenuBar();

		UIManager.put("control", new Color(128, 128, 128));
		UIManager.put("info", new Color(128, 128, 128));
		UIManager.put("nimbusBase", new Color(18, 30, 49));
		UIManager.put("nimbusAlertYellow", new Color(248, 187, 0));
		UIManager.put("nimbusDisabledText", new Color(128, 128, 128));
		UIManager.put("nimbusFocus", new Color(115, 164, 209));
		UIManager.put("nimbusGreen", new Color(176, 179, 50));
		UIManager.put("nimbusInfoBlue", new Color(66, 139, 221));
		UIManager.put("nimbusLightBackground", new Color(18, 30, 49));
		UIManager.put("nimbusOrange", new Color(191, 98, 4));
		UIManager.put("nimbusRed", new Color(169, 46, 34));
		UIManager.put("nimbusSelectedText", new Color(255, 255, 255));
		UIManager.put("nimbusSelectionBackground", new Color(104, 93, 156));
		UIManager.put("text", new Color(230, 230, 230));

		for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
			if ("Nimbus".equals(info.getName())) {
				try {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException e) {
					e.printStackTrace();
				}
				break;
			}
		}

		b.setBackground(Config.MENU_COLOR);
		b.setBorder(null);
		JMenu fmenu = new JMenu("File");
		fmenu.setForeground(Color.WHITE);
		b.add(fmenu);
		JMenuItem open = new JMenuItem("Open");
		JMenuItem save = new JMenuItem("Save");

		fmenu.add(open);
		fmenu.add(save);

		JMenu hmenu = new JMenu("Help");
		hmenu.setForeground(Color.WHITE);
		b.add(hmenu);

		JMenuItem contact = new JMenuItem("Contact");
		hmenu.add(contact);

		open.addActionListener(new MenuItemListener());
		save.addActionListener(new MenuItemListener());
		contact.addActionListener(new MenuItemListener());

		add(b, BorderLayout.NORTH);
	}

}
