package lad.api.components;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import lad.api.Config;

public class Frame extends JFrame {

	/**
	 * @author Kenneth M.
	 */
	private static final long serialVersionUID = 1L;
	public static int appWidth, appHeight;
	private static Point initialClick;

	public static JFileChooser fc = null;

	public Frame() {
		System.out.println("Loading Frame...");
		/**
		 * Sets the size of the frame and saves width and height in two
		 * integers.
		 */
		setPreferredSize(Config.FRAME_SIZE);
		appWidth = (int) getPreferredSize().getWidth();
		appHeight = (int) getPreferredSize().getHeight();
		/**
		 * Removes frame decoration (OS Title Bar)
		 */
		setUndecorated(true);
		/**
		 * Sets the Title of the Frame, and sets the layout to null so we can
		 * use absolute positioning on our components
		 */
		setTitle("Drop Editor - Developed by Ken");
		setLayout(null);

		setBackground(Config.BACKGROUND_COLOR);
		setContentPane(new JLabel());

		addMenuBar();
		addFileChooser();

		addMouseListener();
		/** Pack all of our components into the frame */
		pack();
	}

	/**
	 * Adds our custom menu bar
	 */
	private void addMenuBar() {
		TitleBar bar = new TitleBar(this);
		bar.setBounds(0, 0, appWidth, 25);
		add(bar);
		MenuBar mbar = new MenuBar(this);
		mbar.setBounds(0, 25, appWidth, 25);
		add(mbar);
	}

	/**
	 * Add's our FileChooser component
	 */
	private void addFileChooser() {
		fc = new JFileChooser();
		add(fc);
	}

	/**
	 * Adds our mouse listener for moving the Editor window
	 */
	private void addMouseListener() {
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				initialClick = e.getPoint();
				getComponentAt(initialClick);
			}

		});
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				/**
				 * Handles Frame dragging
				 */
				int thisX = getLocation().x;
				int thisY = getLocation().y;

				if (initialClick != null) {
					int xMoved = (thisX + e.getX()) - (thisX + initialClick.x);
					int yMoved = (thisY + e.getY()) - (thisY + initialClick.y);

					int X = thisX + xMoved;
					int Y = thisY + yMoved;
					setLocation(X, Y);
				}
			}
		});
	}

}
