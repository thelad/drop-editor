package lad.api.components;

import java.awt.AlphaComposite;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Icon;
import javax.swing.JButton;

import lad.api.Editor;
import lad.api.Config;

public class Button extends JButton implements MouseListener {
	/**
	 * @author Kenneth M.
	 */
	private static final long serialVersionUID = -5287805226080046258L;
	private double opacity = 1.0;
	
	public Button(Icon image) {
		super(image);
		
		setBorderPainted(false);
		setFocusable(false);
		addMouseListener(this);
		
		
	}	
	
	@Override
	public void paint(Graphics g) 
	{
	/* This is so that we can manipulate the button opacity and use hover effects */
	    Graphics2D g2 = (Graphics2D) g.create();
	    g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) opacity));
	    super.paint(g2);
	    g2.dispose();
	}
	
	public Button(String name) {
		super(name);
		setForeground(Config.DEFAULT_BTN_COLOR);
		setBorderPainted(false);
		setFocusable(false);
		addMouseListener(this);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent me) {
		Editor.app.setCursor(new Cursor(Cursor.HAND_CURSOR));
		opacity = 0.7;
	}

	@Override
	public void mouseExited(MouseEvent me) {
		Editor.app.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		opacity = 1.0;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		opacity = 0.5;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		opacity = 1.0;
		
	}

}
