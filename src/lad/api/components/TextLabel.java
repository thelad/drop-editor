package lad.api.components;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JLabel;

import lad.api.utils.Utils;

public class TextLabel extends JLabel {

	/**
	 * @author Kenneth M.
	 */
	private static final long serialVersionUID = 6388844078728548652L;
	
	public TextLabel(String text, int fontSize) {
    	super(text);
    	setForeground(Color.WHITE);
    	Utils.setFont(this, "impact.ttf", fontSize);
    }
    
    public void paintComponent(Graphics g) {
        ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
        super.paintComponent(g);
    }
    
    
}
