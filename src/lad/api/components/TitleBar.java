package lad.api.components;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import lad.api.listeners.ButtonListener;
import lad.api.utils.Utils;

public class TitleBar extends JPanel {
	/**
	 * Author: Kenneth M.
	 */
	private static final long serialVersionUID = 987361746473061347L;

	public static Button mini = new Button(Utils.getImageIcon("mini.png"));
	public static Button exit = new Button(Utils.getImageIcon("exit.png"));

	public static Image menuBg = Utils.getImage("menubar.png");

	/**
	 * Creates our custom Titlebar used in place of the default OS Title Bar
	 */
	public TitleBar(JFrame frame) {
		setLayout(null);

		setBounds(0, 0, Frame.appWidth, 25);
		setBackground(new Color(166, 39, 35));

		/* Creates the exit button */
		exit.setOpaque(false);
		exit.setContentAreaFilled(false);
		exit.setActionCommand("X");
		exit.addActionListener(new ButtonListener());
		exit.setBounds(Frame.appWidth - 25, 0, 25, 25);
		add(exit);
		exit.setName("X");

		/* Creates the minimize button */
		mini.setOpaque(false);
		mini.setContentAreaFilled(false);
		mini.setActionCommand("_");
		mini.addActionListener(new ButtonListener());
		mini.setBounds(Frame.appWidth - 50, 0, 25, 25);
		add(mini);
		mini.setName("_");

		/* Creates the Frame title label */
		JLabel title = new TextLabel("Drop Editor - Developed by Ken", 12);
		title.setBounds(40, 0, Frame.appWidth, 25);

		/* Creates the FavIcon */
		JLabel icon = new JLabel(Utils.getImageIcon("favicon.png"));
		icon.setBounds(3, -2, 24, 28);
		add(icon);
		add(title);

	}

	@Override
	protected void paintComponent(Graphics g) {
		/**
		 * Draws our custom menu background
		 */
		super.paintComponent(g);
		g.drawImage(menuBg, 0, 0, null);
	}
}
