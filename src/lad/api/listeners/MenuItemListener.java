package lad.api.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import lad.api.components.Frame;
import lad.api.utils.Utils;
import lad.api.utils.XMLReader;

/**
 * 
 * @author Kenneth M.
 *
 */
public class MenuItemListener implements ActionListener {
	int returnVal;

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Open":
			returnVal = Frame.fc.showOpenDialog(Frame.fc);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = Frame.fc.getSelectedFile();
				String extension = "";
				int i = file.getName().lastIndexOf('.');
				if (i > 0) {
					extension = file.getName().substring(i + 1);
				}
				if (extension.equalsIgnoreCase("xml")) {
					System.out.println("Reading the file");
					try {
						new XMLReader(file);
					} catch (ParserConfigurationException | SAXException | IOException e1) {
						e1.printStackTrace();
					}
				} else {
					Utils.showError("Invalid File Extension", "This program only reads XML files!");
				}
			} else {
				System.out.println("Open command canceled by user");
			}
			break;

		case "Save":
			returnVal = Frame.fc.showSaveDialog(Frame.fc);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = Frame.fc.getSelectedFile();
				// This is where a real application would save the file.
				System.out.println("Saving: " + file.getName());
			} else {
				System.out.println("Save command canceled by user.");
			}
			break;

		case "Contact":
			Utils.openWebpage("https://www.rune-server.ee/members/ken/");
			break;
		}
	}

}
