package lad.api.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

import lad.api.Editor;

/**
 * 
 * @author Kenneth M.
 *
 */
public class ButtonListener implements ActionListener {
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()) {
		
		case "_":
			/* Minimizes the application */
			Editor.app.setState(JFrame.ICONIFIED);
			break;
			
		case "X":
			/* Exits the application */
			System.exit(0);
			break;
			
		default:
			/* Outputs the action command for unhandled buttons */
			System.out.println(e.getActionCommand());
			break;
			
		}
	}

}